#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	clock_t CLOCKS_PER_MS = CLOCKS_PER_SEC / 1000;

	//allocate 1GB
	char* large_buffer = (char*)malloc(1073741824);
	char* large_buffer_cpy = (char*)malloc(1073741824);

	clock_t start = clock();
	memcpy(large_buffer_cpy, large_buffer, 1073741824);
	clock_t end = clock();

	clock_t elapsed = ((double) (end - start)) / CLOCKS_PER_MS + 0.5;

	printf("Time elapsed: %Lf\n", (long double)elapsed);
}