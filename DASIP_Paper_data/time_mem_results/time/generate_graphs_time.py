import datetime
import sys
import numpy as np
import matplotlib.pyplot as plt
import math

def normalize(v):
    l2 = math.sqrt(v[0] * v[0] + v[1] * v[1])
    return (v[0] / l2, v[1] / l2)

def dot(v1, v2):
	return v1[0] * v2[0] + v1[1] * v2[1]

def compute_parallel_speedup_err(data1, data1p, data2, data2p):
	data1 = [x[0] for x in data1]
	data2 = [x[0] for x in data2]
	data1p = [x[0] for x in data1p]
	data2p = [x[0] for x in data2p]

	grad1 = []
	grad2 = []
	err = []

	for i, val in enumerate(data1):
		g1 = data1[i]/data1p[i]
		g2 = data2[i]/data2p[i]

		e = abs(1 - g2/g1)

		grad1.append(g1)
		grad2.append(g2)
		err.append(e)

	return err, grad1, grad2

def compute_grad_err(data1, data2, xvals):
	data1 = [x[0] for x in data1]
	data2 = [x[0] for x in data2]
	
	last1 = data1[0]
	last2 = data2[0]
	lastx = xvals[0]
	grad1 = []
	grad2 = []
	err = []

	for i, val in enumerate(data1):
		if i == 0:
			continue

		xr = xvals[i] - lastx
		#g1 = math.acos(dot(normalize((xr, (val - last1))), (1, 0)))
		#g2 = math.acos(dot(normalize((xr, (data2[i] - last2))), (1, 0)))

		g1 = val/last1
		g2 = data2[i]/last2

		last1 = val
		last2 = data2[i]
		lastx = xvals[i]

		e = abs(g1 - g2)

		grad1.append(g1)
		grad2.append(g2)
		err.append(e)

	return err, grad1, grad2

def print_stats(err, g1, g2, name):
	print("---------------------------------------------------")
	print(name + " stats:")
	print("Average gradient error: " + str(np.mean(err)))
	print("Grad err: " + str(err))
	print("Measured grad: " + str(g1))
	print("Estimated grad: " + str(g2))

def print_pstats(err, g1, g2, name):
	print("---------------------------------------------------")
	print(name + " stats:")
	print("Average gradient error: " + str(np.mean(err)))
	print("Grad err: " + str(err))
	print("Measured grad: " + str(g1))
	print("Estimated grad: " + str(g2))

actual_grid_sizes = [512*512, 1024*1024, 1536*1536, 2048*2048, 2560*2560]
actual_visibilities = [130816*10, 130816*15, 130816*20, 130816*25, 130816*30]
actual_min_cycles = [50, 100, 150, 200, 250]

readable_grid_sizes = [512, 1024, 1536, 2048, 2560]
readable_visibilities = [10, 15, 20, 25, 30]
readable_min_cycles = [50, 100, 150, 200, 250]


graph_names = {}
graph_names["4coreDFT"] = '4core DFT measured'
graph_names["4coreFFT"] = '4core FFT measured'
graph_names["4coreG2G"] = '4core G2G measured'
graph_names["1coreDFT"] = '1core DFT measured'
graph_names["1coreFFT"] = '1core FFT measured'
graph_names["1coreG2G"] = '1core G2G measured'
graph_names["4coreDFTEst"] = '4core DFT estimated'
graph_names["4coreFFTEst"] = '4core FFT estimated'
graph_names["4coreG2GEst"] = '4core G2G estimated'
graph_names["1coreDFTEst"] = '1core DFT estimated'
graph_names["1coreFFTEst"] = '1core FFT estimated'
graph_names["1coreG2GEst"] = '1core G2G estimated'

graph_cols = {}
graph_cols["4coreDFT"] = 'mediumblue'
graph_cols["4coreFFT"] = 'darkgreen'
graph_cols["4coreG2G"] = 'crimson'
graph_cols["1coreDFT"] = 'cornflowerblue'
graph_cols["1coreFFT"] = 'mediumseagreen'
graph_cols["1coreG2G"] = 'palevioletred'
graph_cols["4coreDFTEst"] = 'mediumblue'
graph_cols["4coreFFTEst"] = 'darkgreen'
graph_cols["4coreG2GEst"] = 'crimson'
graph_cols["1coreDFTEst"] = 'cornflowerblue'
graph_cols["1coreFFTEst"] = 'mediumseagreen'
graph_cols["1coreG2GEst"] = 'palevioletred'


graph_linestyles = {}
graph_linestyles["4coreDFT"] = '-'
graph_linestyles["4coreFFT"] = '-'
graph_linestyles["4coreG2G"] = '-'
graph_linestyles["1coreDFT"] = '-'
graph_linestyles["1coreFFT"] = '-'
graph_linestyles["1coreG2G"] = '-'
graph_linestyles["4coreDFTEst"] = '--'
graph_linestyles["4coreFFTEst"] = '--'
graph_linestyles["4coreG2GEst"] = '--'
graph_linestyles["1coreDFTEst"] = '--'
graph_linestyles["1coreFFTEst"] = '--'
graph_linestyles["1coreG2GEst"] = '--'

graphs_files = {}

graphs_files["Visibilities"] = {}
graphs_files["Visibilities"]["4coreDFT"] = []
graphs_files["Visibilities"]["4coreFFT"] = []
graphs_files["Visibilities"]["4coreG2G"] = []
graphs_files["Visibilities"]["1coreDFT"] = []
graphs_files["Visibilities"]["1coreFFT"] = []
graphs_files["Visibilities"]["1coreG2G"] = []
graphs_files["Visibilities"]["4coreDFTEst"] = []
graphs_files["Visibilities"]["4coreFFTEst"] = []
graphs_files["Visibilities"]["4coreG2GEst"] = []
graphs_files["Visibilities"]["1coreDFTEst"] = []
graphs_files["Visibilities"]["1coreFFTEst"] = []
graphs_files["Visibilities"]["1coreG2GEst"] = []

for vis in readable_visibilities:
	graphs_files["Visibilities"]["4coreDFT"].append("4core/pipeline_timings_dft_vis" + str(vis))
	graphs_files["Visibilities"]["4coreFFT"].append("4core/pipeline_timings_fft_vis" + str(vis))
	graphs_files["Visibilities"]["4coreG2G"].append("4core/pipeline_timings_g2g_vis" + str(vis))
	graphs_files["Visibilities"]["1coreDFT"].append("1core/pipeline_timings_dft_vis" + str(vis))
	graphs_files["Visibilities"]["1coreFFT"].append("1core/pipeline_timings_fft_vis" + str(vis))
	graphs_files["Visibilities"]["1coreG2G"].append("1core/pipeline_timings_g2g_vis" + str(vis))

	graphs_files["Visibilities"]["4coreDFTEst"].append("4core/pipeline_timings_dft_vis" + str(vis) + "_est")
	graphs_files["Visibilities"]["4coreFFTEst"].append("4core/pipeline_timings_fft_vis" + str(vis) + "_est")
	graphs_files["Visibilities"]["4coreG2GEst"].append("4core/pipeline_timings_g2g_vis" + str(vis) + "_est")
	graphs_files["Visibilities"]["1coreDFTEst"].append("1core/pipeline_timings_dft_vis" + str(vis) + "_est")
	graphs_files["Visibilities"]["1coreFFTEst"].append("1core/pipeline_timings_fft_vis" + str(vis) + "_est")
	graphs_files["Visibilities"]["1coreG2GEst"].append("1core/pipeline_timings_g2g_vis" + str(vis) + "_est")

graphs_files["Grid Cells"] = {}
graphs_files["Grid Cells"]["4coreDFT"] = []
graphs_files["Grid Cells"]["4coreFFT"] = []
graphs_files["Grid Cells"]["4coreG2G"] = []
graphs_files["Grid Cells"]["1coreDFT"] = []
graphs_files["Grid Cells"]["1coreFFT"] = []
graphs_files["Grid Cells"]["1coreG2G"] = []
graphs_files["Grid Cells"]["4coreDFTEst"] = []
graphs_files["Grid Cells"]["4coreFFTEst"] = []
graphs_files["Grid Cells"]["4coreG2GEst"] = []
graphs_files["Grid Cells"]["1coreDFTEst"] = []
graphs_files["Grid Cells"]["1coreFFTEst"] = []
graphs_files["Grid Cells"]["1coreG2GEst"] = []

for gridsize in readable_grid_sizes:
	graphs_files["Grid Cells"]["4coreDFT"].append("4core/pipeline_timings_dft_grid" + str(gridsize))
	graphs_files["Grid Cells"]["4coreFFT"].append("4core/pipeline_timings_fft_grid" + str(gridsize))
	graphs_files["Grid Cells"]["4coreG2G"].append("4core/pipeline_timings_g2g_grid" + str(gridsize))
	graphs_files["Grid Cells"]["1coreDFT"].append("1core/pipeline_timings_dft_grid" + str(gridsize))
	graphs_files["Grid Cells"]["1coreFFT"].append("1core/pipeline_timings_fft_grid" + str(gridsize))
	graphs_files["Grid Cells"]["1coreG2G"].append("1core/pipeline_timings_g2g_grid" + str(gridsize))

	graphs_files["Grid Cells"]["4coreDFTEst"].append("4core/pipeline_timings_dft_grid" + str(gridsize) + "_est")
	graphs_files["Grid Cells"]["4coreFFTEst"].append("4core/pipeline_timings_fft_grid" + str(gridsize) + "_est")
	graphs_files["Grid Cells"]["4coreG2GEst"].append("4core/pipeline_timings_g2g_grid" + str(gridsize) + "_est")
	graphs_files["Grid Cells"]["1coreDFTEst"].append("1core/pipeline_timings_dft_grid" + str(gridsize) + "_est")
	graphs_files["Grid Cells"]["1coreFFTEst"].append("1core/pipeline_timings_fft_grid" + str(gridsize) + "_est")
	graphs_files["Grid Cells"]["1coreG2GEst"].append("1core/pipeline_timings_g2g_grid" + str(gridsize) + "_est")

graphs_files["Sources per Major Cycle"] = {}
graphs_files["Sources per Major Cycle"]["4coreDFT"] = []
graphs_files["Sources per Major Cycle"]["4coreFFT"] = []
graphs_files["Sources per Major Cycle"]["4coreG2G"] = []
graphs_files["Sources per Major Cycle"]["1coreDFT"] = []
graphs_files["Sources per Major Cycle"]["1coreFFT"] = []
graphs_files["Sources per Major Cycle"]["1coreG2G"] = []
graphs_files["Sources per Major Cycle"]["4coreDFTEst"] = []
graphs_files["Sources per Major Cycle"]["4coreFFTEst"] = []
graphs_files["Sources per Major Cycle"]["4coreG2GEst"] = []
graphs_files["Sources per Major Cycle"]["1coreDFTEst"] = []
graphs_files["Sources per Major Cycle"]["1coreFFTEst"] = []
graphs_files["Sources per Major Cycle"]["1coreG2GEst"] = []

for mincycle in readable_min_cycles:
	graphs_files["Sources per Major Cycle"]["4coreDFT"].append("4core/pipeline_timings_dft_mincycle" + str(mincycle))
	graphs_files["Sources per Major Cycle"]["4coreFFT"].append("4core/pipeline_timings_fft_mincycle" + str(mincycle))
	graphs_files["Sources per Major Cycle"]["4coreG2G"].append("4core/pipeline_timings_g2g_mincycle" + str(mincycle))
	graphs_files["Sources per Major Cycle"]["1coreDFT"].append("1core/pipeline_timings_dft_mincycle" + str(mincycle))
	graphs_files["Sources per Major Cycle"]["1coreFFT"].append("1core/pipeline_timings_fft_mincycle" + str(mincycle))
	graphs_files["Sources per Major Cycle"]["1coreG2G"].append("1core/pipeline_timings_g2g_mincycle" + str(mincycle))

	graphs_files["Sources per Major Cycle"]["4coreDFTEst"].append("4core/pipeline_timings_dft_mincycle" + str(mincycle) + "_est")
	graphs_files["Sources per Major Cycle"]["4coreFFTEst"].append("4core/pipeline_timings_fft_mincycle" + str(mincycle) + "_est")
	graphs_files["Sources per Major Cycle"]["4coreG2GEst"].append("4core/pipeline_timings_g2g_mincycle" + str(mincycle) + "_est")
	graphs_files["Sources per Major Cycle"]["1coreDFTEst"].append("1core/pipeline_timings_dft_mincycle" + str(mincycle) + "_est")
	graphs_files["Sources per Major Cycle"]["1coreFFTEst"].append("1core/pipeline_timings_fft_mincycle" + str(mincycle) + "_est")
	graphs_files["Sources per Major Cycle"]["1coreG2GEst"].append("1core/pipeline_timings_g2g_mincycle" + str(mincycle) + "_est")


graphs_dat = {}

graphs_dat["Visibilities"] = {}
graphs_dat["Visibilities"]["4coreDFT"] = []
graphs_dat["Visibilities"]["4coreFFT"] = []
graphs_dat["Visibilities"]["4coreG2G"] = []
graphs_dat["Visibilities"]["1coreDFT"] = []
graphs_dat["Visibilities"]["1coreFFT"] = []
graphs_dat["Visibilities"]["1coreG2G"] = []

graphs_dat["Visibilities"]["4coreDFTEst"] = []
graphs_dat["Visibilities"]["4coreFFTEst"] = []
graphs_dat["Visibilities"]["4coreG2GEst"] = []
graphs_dat["Visibilities"]["1coreDFTEst"] = []
graphs_dat["Visibilities"]["1coreFFTEst"] = []
graphs_dat["Visibilities"]["1coreG2GEst"] = []


graphs_dat["Grid Cells"] = {}
graphs_dat["Grid Cells"]["4coreDFT"] = []
graphs_dat["Grid Cells"]["4coreFFT"] = []
graphs_dat["Grid Cells"]["4coreG2G"] = []
graphs_dat["Grid Cells"]["1coreDFT"] = []
graphs_dat["Grid Cells"]["1coreFFT"] = []
graphs_dat["Grid Cells"]["1coreG2G"] = []

graphs_dat["Grid Cells"]["4coreDFTEst"] = []
graphs_dat["Grid Cells"]["4coreFFTEst"] = []
graphs_dat["Grid Cells"]["4coreG2GEst"] = []
graphs_dat["Grid Cells"]["1coreDFTEst"] = []
graphs_dat["Grid Cells"]["1coreFFTEst"] = []
graphs_dat["Grid Cells"]["1coreG2GEst"] = []

graphs_dat["Sources per Major Cycle"] = {}
graphs_dat["Sources per Major Cycle"]["4coreDFT"] = []
graphs_dat["Sources per Major Cycle"]["4coreFFT"] = []
graphs_dat["Sources per Major Cycle"]["4coreG2G"] = []
graphs_dat["Sources per Major Cycle"]["1coreDFT"] = []
graphs_dat["Sources per Major Cycle"]["1coreFFT"] = []
graphs_dat["Sources per Major Cycle"]["1coreG2G"] = []

graphs_dat["Sources per Major Cycle"]["4coreDFTEst"] = []
graphs_dat["Sources per Major Cycle"]["4coreFFTEst"] = []
graphs_dat["Sources per Major Cycle"]["4coreG2GEst"] = []
graphs_dat["Sources per Major Cycle"]["1coreDFTEst"] = []
graphs_dat["Sources per Major Cycle"]["1coreFFTEst"] = []
graphs_dat["Sources per Major Cycle"]["1coreG2GEst"] = []

for graph in graphs_dat:
	for config in graphs_dat[graph]:
		for filename in graphs_files[graph][config]:
			file = open(filename, "r")
			timestamps = file.read().split("\n")
			seconds = [(datetime.datetime.strptime(ts, "%Mm%S.%fs") - datetime.datetime(1900, 1, 1)).total_seconds() for ts in timestamps]
			average = np.average(seconds)
			stdev = np.std(seconds)
			graphs_dat[graph][config].append((average,stdev))


print("#######################Visibility stats#######################")
err, g1, g2 = compute_grad_err(graphs_dat["Visibilities"]["1coreDFT"], graphs_dat["Visibilities"]["1coreDFTEst"], actual_visibilities)
print_stats(err, g1, g2, "1coreDFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Visibilities"]["1coreFFT"], graphs_dat["Visibilities"]["1coreFFTEst"], actual_visibilities)
print_stats(err, g1, g2, "1coreFFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Visibilities"]["1coreG2G"], graphs_dat["Visibilities"]["1coreG2GEst"], actual_visibilities)
print_stats(err, g1, g2, "1coreG2G vis")
err, g1, g2 = compute_grad_err(graphs_dat["Visibilities"]["4coreDFT"], graphs_dat["Visibilities"]["4coreDFTEst"], actual_visibilities)
print_stats(err, g1, g2, "4coreDFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Visibilities"]["4coreFFT"], graphs_dat["Visibilities"]["4coreFFTEst"], actual_visibilities)
print_stats(err, g1, g2, "4coreFFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Visibilities"]["4coreG2G"], graphs_dat["Visibilities"]["4coreG2GEst"], actual_visibilities)
print_stats(err, g1, g2, "4coreG2G vis")
print("\n\n\n")

print("#######################Grid cells stats#######################")
err, g1, g2 = compute_grad_err(graphs_dat["Grid Cells"]["1coreDFT"], graphs_dat["Grid Cells"]["1coreDFTEst"], actual_grid_sizes)
print_stats(err, g1, g2, "1coreDFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Grid Cells"]["1coreFFT"], graphs_dat["Grid Cells"]["1coreFFTEst"], actual_grid_sizes)
print_stats(err, g1, g2, "1coreFFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Grid Cells"]["1coreG2G"], graphs_dat["Grid Cells"]["1coreG2GEst"], actual_grid_sizes)
print_stats(err, g1, g2, "1coreG2G vis")
err, g1, g2 = compute_grad_err(graphs_dat["Grid Cells"]["4coreDFT"], graphs_dat["Grid Cells"]["4coreDFTEst"], actual_grid_sizes)
print_stats(err, g1, g2, "4coreDFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Grid Cells"]["4coreFFT"], graphs_dat["Grid Cells"]["4coreFFTEst"], actual_grid_sizes)
print_stats(err, g1, g2, "4coreFFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Grid Cells"]["4coreG2G"], graphs_dat["Grid Cells"]["4coreG2GEst"], actual_grid_sizes)
print_stats(err, g1, g2, "4coreG2G vis")
print("\n\n\n")

print("#######################Min cycles stats#######################")
err, g1, g2 = compute_grad_err(graphs_dat["Sources per Major Cycle"]["1coreDFT"], graphs_dat["Sources per Major Cycle"]["1coreDFTEst"], actual_min_cycles)
print_stats(err, g1, g2, "1coreDFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Sources per Major Cycle"]["1coreFFT"], graphs_dat["Sources per Major Cycle"]["1coreFFTEst"], actual_min_cycles)
print_stats(err, g1, g2, "1coreFFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Sources per Major Cycle"]["1coreG2G"], graphs_dat["Sources per Major Cycle"]["1coreG2GEst"], actual_min_cycles)
print_stats(err, g1, g2, "1coreG2G vis")
err, g1, g2 = compute_grad_err(graphs_dat["Sources per Major Cycle"]["4coreDFT"], graphs_dat["Sources per Major Cycle"]["4coreDFTEst"], actual_min_cycles)
print_stats(err, g1, g2, "4coreDFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Sources per Major Cycle"]["4coreFFT"], graphs_dat["Sources per Major Cycle"]["4coreFFTEst"], actual_min_cycles)
print_stats(err, g1, g2, "4coreFFT vis")
err, g1, g2 = compute_grad_err(graphs_dat["Sources per Major Cycle"]["4coreG2G"], graphs_dat["Sources per Major Cycle"]["4coreG2GEst"], actual_min_cycles)
print_stats(err, g1, g2, "4coreG2G vis")
print("\n\n\n")


print("#######################Parallelization stats#######################")
err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Visibilities"]["1coreDFT"], graphs_dat["Visibilities"]["4coreDFT"], \
	graphs_dat["Visibilities"]["1coreDFTEst"], graphs_dat["Visibilities"]["4coreDFTEst"])
print_pstats(err, g1, g2, "DFT vis")
err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Visibilities"]["1coreFFT"], graphs_dat["Visibilities"]["4coreFFT"], \
	graphs_dat["Visibilities"]["1coreFFTEst"], graphs_dat["Visibilities"]["4coreFFTEst"])
print_pstats(err, g1, g2, "FFT vis")
err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Visibilities"]["1coreG2G"], graphs_dat["Visibilities"]["4coreG2G"], \
	graphs_dat["Visibilities"]["1coreG2GEst"], graphs_dat["Visibilities"]["4coreG2GEst"])
print_pstats(err, g1, g2, "G2G vis")

err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Grid Cells"]["1coreDFT"], graphs_dat["Grid Cells"]["4coreDFT"], \
	graphs_dat["Grid Cells"]["1coreDFTEst"], graphs_dat["Grid Cells"]["4coreDFTEst"])
print_pstats(err, g1, g2, "DFT gridcells")
err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Grid Cells"]["1coreFFT"], graphs_dat["Grid Cells"]["4coreFFT"], \
	graphs_dat["Grid Cells"]["1coreFFTEst"], graphs_dat["Grid Cells"]["4coreFFTEst"])
print_pstats(err, g1, g2, "FFT gridcells")
err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Grid Cells"]["1coreG2G"], graphs_dat["Grid Cells"]["4coreG2G"], \
	graphs_dat["Grid Cells"]["1coreG2GEst"], graphs_dat["Grid Cells"]["4coreG2GEst"])
print_pstats(err, g1, g2, "G2G gridcells")

err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Sources per Major Cycle"]["1coreDFT"], graphs_dat["Sources per Major Cycle"]["4coreDFT"], \
	graphs_dat["Sources per Major Cycle"]["1coreDFTEst"], graphs_dat["Sources per Major Cycle"]["4coreDFTEst"])
print_pstats(err, g1, g2, "DFT mincyc")
err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Sources per Major Cycle"]["1coreFFT"], graphs_dat["Sources per Major Cycle"]["4coreFFT"], \
	graphs_dat["Sources per Major Cycle"]["1coreFFTEst"], graphs_dat["Sources per Major Cycle"]["4coreFFTEst"])
print_pstats(err, g1, g2, "FFT mincyc")
err, g1, g2 = compute_parallel_speedup_err(graphs_dat["Sources per Major Cycle"]["1coreG2G"], graphs_dat["Sources per Major Cycle"]["4coreG2G"], \
	graphs_dat["Sources per Major Cycle"]["1coreG2GEst"], graphs_dat["Sources per Major Cycle"]["4coreG2GEst"])
print_pstats(err, g1, g2, "G2G mincyc")

figs, axes = plt.subplots(1)
axes.tick_params(axis='x', labelsize=26)
axes.tick_params(axis='y', labelsize=26)
axes.set_ylabel("Time (s)", fontsize=32)
axes.xaxis.get_offset_text().set_fontsize(26)
plt.gcf().set_size_inches(25, 10)


axes.set_xlabel("Number of Visibilities", fontsize = 32)
axes.set_title('Time vs Num Visibilities', fontsize = 40)
for config in graphs_dat["Visibilities"]:
	x = actual_visibilities
	curr_config_dat = graphs_dat["Visibilities"][config]
	axes.plot(x, [y[0] for y in curr_config_dat], color=graph_cols[config], linestyle=graph_linestyles[config], label=graph_names[config], linewidth=5)
	axes.legend(loc='upper left', ncol=2,  prop = {"size": 30})
plt.savefig("vis_time.png",bbox_inches='tight', dpi=300)

# axes.set_xlabel("Number of Grid Cells", fontsize=32)
# axes.set_title('Time vs Grid Cells', fontsize = 40)
# for config in graphs_dat["Grid Cells"]:
# 	x = actual_grid_sizes
# 	curr_config_dat = graphs_dat["Grid Cells"][config]
# 	axes.plot(x, [y[0] for y in curr_config_dat], color=graph_cols[config], linestyle=graph_linestyles[config], label=graph_names[config], linewidth=5)
# 	axes.legend(loc='upper left', ncol=2,  prop = {"size": 30})
# plt.savefig("gridcells_time.png",bbox_inches='tight', dpi=300)


# axes.set_title('Time vs Minor Cycles', fontsize = 40)
# axes.set_xlabel("Minor Cycles", fontsize = 32)

# for config in graphs_dat["Sources per Major Cycle"]:
# 	x = actual_min_cycles
# 	curr_config_dat = graphs_dat["Sources per Major Cycle"][config]
# 	axes.plot(x, [y[0] for y in curr_config_dat], color=graph_cols[config], linestyle=graph_linestyles[config], label=graph_names[config], linewidth=5)
# 	axes.legend(loc='upper left', ncol=2, prop = {"size": 30})
# plt.savefig("mincycles_time.png",bbox_inches='tight', dpi=300)


#plt.show()