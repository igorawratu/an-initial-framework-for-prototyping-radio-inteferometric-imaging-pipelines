<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns">
    <key attr.name="parameters" for="graph" id="parameters"/>
    <key attr.name="variables" for="graph" id="variables"/>
    <key attr.name="arguments" for="node" id="arguments"/>
    <key attr.name="name" attr.type="string" for="graph"/>
    <key attr.name="graph_desc" attr.type="string" for="node"/>
    <key attr.name="delay" attr.type="string" for="edge"/>
    <graph edgedefault="directed">
        <data key="name">generic_top</data>
        <node expr="2048" id="GRID_SIZE" kind="param"/>
        <node expr="NUM_MINOR_CYCLES" id="MAX_SOURCES" kind="param"/>
        <node expr="NUM_RECEIVERS*(NUM_RECEIVERS-1)/2"
            id="NUM_BASELINES" kind="param"/>
        <node expr="17" id="NUM_GRIDDING_KERNELS" kind="param"/>
        <node expr="512" id="NUM_RECEIVERS" kind="param"/>
        <node expr="3924480" id="NUM_VISIBILITIES" kind="param"/>
        <node expr="108800" id="TOTAL_GRIDDING_KERNEL_SAMPLES" kind="param"/>
        <node expr="5" id="NUM_MAJOR_CYCLES" kind="param"/>
        <node expr="200" id="NUM_MINOR_CYCLES" kind="param"/>
        <node expr="0" id="GAUSSIAN_CLEAN_PSF" kind="param"/>
        <node expr="0" id="Zero" kind="param"/>
        <node expr="17" id="NUM_DEGRIDDING_KERNELS" kind="param"/>
        <node expr="108800" id="TOTAL_DEGRIDDING_KERNEL_SAMPLES" kind="param"/>
        <node expr="16" id="OVERSAMPLING_FACTOR" kind="param"/>
        <node expr="4" id="SLICES" kind="param"/>
        <node id="psi" kind="actor">
            <data key="graph_desc">Algo/sep_psi.pi</data>
            <port kind="cfg_input" name="GRID_SIZE"/>
            <port kind="cfg_input" name="NUM_MINOR_CYCLES"/>
            <port kind="cfg_input" name="MAX_SOURCES"/>
            <port annotation="NONE" expr="1" kind="input" name="config"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="psf"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="delta_image"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="input_model"/>
            <port annotation="NONE" expr="1" kind="input" name="partial_psf_halfdims"/>
            <port annotation="NONE" expr="1" kind="input" name="cycle"/>
            <port annotation="NONE" expr="MAX_SOURCES"
                kind="output" name="source_list"/>
            <port annotation="NONE" expr="1" kind="output" name="num_sources_out"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="image_estimate"/>
        </node>
        <node id="save_output" kind="actor">
            <data key="graph_desc">Code/include/major_loop_iter.h</data>
            <loop name="save_output">
                <param direction="IN" isConfig="true"
                    name="GRID_SIZE" type="int"/>
                <param direction="IN" isConfig="false"
                    name="residual" type="PRECISION"/>
                <param direction="IN" isConfig="false"
                    name="model" type="PRECISION"/>
                <param direction="IN" isConfig="false"
                    name="clean_psf" type="PRECISION"/>
                <param direction="IN" isConfig="false"
                    name="clean_psf_halfdims" type="int2"/>
                <param direction="IN" isConfig="false" name="psf" type="PRECISION"/>
                <param direction="IN" isConfig="false"
                    name="config" type="Config"/>
                <param direction="IN" isConfig="false"
                    name="cycle" type="int"/>
            </loop>
            <port kind="cfg_input" name="GRID_SIZE"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="residual"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="model"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="clean_psf"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="psf"/>
            <port annotation="NONE" expr="1" kind="input" name="config"/>
            <port annotation="NONE" expr="1" kind="input" name="cycle"/>
            <port annotation="NONE" expr="1" kind="input" name="clean_psf_halfdims"/>
        </node>
        <node id="Broadcast_image_estimate" kind="broadcast">
            <port kind="cfg_input" name="GRID_SIZE"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="input"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="delta"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="save"/>
        </node>
        <node id="Broadcast_residual" kind="broadcast">
            <port kind="cfg_input" name="GRID_SIZE"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="input"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="psi"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="save"/>
        </node>
        <node id="iterator" kind="actor">
            <data key="graph_desc">Code/include/major_loop_actor.h</data>
            <loop name="iterator">
                <param direction="IN" isConfig="true" name="ITER" type="int"/>
                <param direction="IN" isConfig="true"
                    name="START" type="int"/>
                <param direction="OUT" isConfig="false"
                    name="cycle_out" type="int"/>
            </loop>
            <port kind="cfg_input" name="ITER"/>
            <port kind="cfg_input" name="START"/>
            <port annotation="NONE" expr="ITER" kind="output" name="cycle_out"/>
        </node>
        <node id="Broadcast_cycle" kind="broadcast">
            <port annotation="READ_ONLY" expr="1" kind="input" name="input"/>
            <port annotation="WRITE_ONLY" expr="1" kind="output" name="delta"/>
            <port annotation="WRITE_ONLY" expr="1" kind="output" name="psi"/>
            <port annotation="WRITE_ONLY" expr="1" kind="output" name="save"/>
        </node>
        <node id="delta" kind="actor">
            <data key="graph_desc">Algo/s2s_delta.pi</data>
            <port kind="cfg_input" name="NUM_GRIDDING_KERNELS"/>
            <port kind="cfg_input" name="TOTAL_GRIDDING_KERNEL_SAMPLES"/>
            <port kind="cfg_input" name="NUM_VISIBILITIES"/>
            <port kind="cfg_input" name="NUM_RECEIVERS"/>
            <port kind="cfg_input" name="NUM_BASELINES"/>
            <port kind="cfg_input" name="GRID_SIZE"/>
            <port kind="cfg_input" name="MAX_SOURCES"/>
            <port kind="cfg_input" name="NUM_DEGRIDDING_KERNELS"/>
            <port kind="cfg_input" name="OVERSAMPLING_FACTOR"/>
            <port kind="cfg_input" name="TOTAL_DEGRIDDING_KERNEL_SAMPLES"/>
            <port kind="cfg_input" name="SLICES"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="image_estimate"/>
            <port annotation="NONE" expr="NUM_VISIBILITIES"
                kind="input" name="measured_vis"/>
            <port annotation="NONE" expr="MAX_SOURCES"
                kind="input" name="source_list"/>
            <port annotation="NONE" expr="1" kind="input" name="num_sources_in"/>
            <port annotation="NONE" expr="NUM_GRIDDING_KERNELS"
                kind="input" name="gridding_kernel_supports"/>
            <port annotation="NONE"
                expr="TOTAL_GRIDDING_KERNEL_SAMPLES" kind="input" name="gridding_kernels"/>
            <port annotation="NONE" expr="NUM_VISIBILITIES"
                kind="input" name="vis_coords"/>
            <port annotation="NONE" expr="NUM_RECEIVERS"
                kind="input" name="gains"/>
            <port annotation="NONE" expr="NUM_BASELINES"
                kind="input" name="receiver_pairs"/>
            <port annotation="NONE" expr="1" kind="input" name="config"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="psf"/>
            <port annotation="NONE" expr="GRID_SIZE/2"
                kind="input" name="prolate"/>
            <port annotation="NONE" expr="1" kind="input" name="cycle"/>
            <port annotation="NONE" expr="NUM_DEGRIDDING_KERNELS"
                kind="input" name="degridding_kernel_supports"/>
            <port annotation="NONE"
                expr="TOTAL_DEGRIDDING_KERNEL_SAMPLES"
                kind="input" name="degridding_kernels"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="delta_image"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="image_out"/>
        </node>
        <node id="setup_ri_pipeline" kind="actor">
            <data key="graph_desc">Algo/setup_ri_pipeline.pi</data>
            <port kind="cfg_input" name="NUM_GRIDDING_KERNELS"/>
            <port kind="cfg_input" name="GRID_SIZE"/>
            <port kind="cfg_input" name="GAUSSIAN_CLEAN_PSF"/>
            <port kind="cfg_input" name="NUM_RECEIVERS"/>
            <port kind="cfg_input" name="NUM_BASELINES"/>
            <port kind="cfg_input" name="TOTAL_GRIDDING_KERNEL_SAMPLES"/>
            <port kind="cfg_input" name="NUM_VISIBILITIES"/>
            <port kind="cfg_input" name="TOTAL_DEGRIDDING_KERNEL_SAMPLES"/>
            <port kind="cfg_input" name="NUM_DEGRIDDING_KERNELS"/>
            <port annotation="NONE" expr="1" kind="output" name="config"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="clean_psf"/>
            <port annotation="NONE" expr="1" kind="output" name="psf_halfdims"/>
            <port annotation="NONE" expr="NUM_GRIDDING_KERNELS"
                kind="output" name="gridding_kernel_supports"/>
            <port annotation="NONE"
                expr="TOTAL_GRIDDING_KERNEL_SAMPLES"
                kind="output" name="gridding_kernels"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="psf"/>
            <port annotation="NONE" expr="NUM_BASELINES"
                kind="output" name="receiver_pairs"/>
            <port annotation="NONE" expr="NUM_VISIBILITIES"
                kind="output" name="vis_uvw_coords"/>
            <port annotation="NONE" expr="NUM_DEGRIDDING_KERNELS"
                kind="output" name="degridding_kernel_supports"/>
            <port annotation="NONE"
                expr="TOTAL_DEGRIDDING_KERNEL_SAMPLES"
                kind="output" name="degridding_kernels"/>
            <port annotation="NONE" expr="NUM_RECEIVERS"
                kind="output" name="gains"/>
            <port annotation="NONE" expr="NUM_VISIBILITIES"
                kind="output" name="measured_visibilities"/>
            <port annotation="NONE" expr="GRID_SIZE/2"
                kind="output" name="prolate"/>
        </node>
        <node id="Broadcast_config" kind="broadcast">
            <port annotation="READ_ONLY" expr="1" kind="input" name="input"/>
            <port annotation="WRITE_ONLY" expr="1" kind="output" name="delta"/>
            <port annotation="WRITE_ONLY" expr="1" kind="output" name="psi"/>
            <port annotation="WRITE_ONLY" expr="1" kind="output" name="save"/>
        </node>
        <node id="Broadcast_psfhalfdims" kind="broadcast">
            <port annotation="READ_ONLY" expr="1" kind="input" name="input"/>
            <port annotation="WRITE_ONLY" expr="1" kind="output" name="psi"/>
            <port annotation="WRITE_ONLY" expr="1" kind="output" name="save"/>
        </node>
        <node id="Broadcast_psf" kind="broadcast">
            <port kind="cfg_input" name="GRID_SIZE"/>
            <port annotation="READ_ONLY"
                expr="GRID_SIZE*GRID_SIZE" kind="input" name="input"/>
            <port annotation="WRITE_ONLY"
                expr="GRID_SIZE*GRID_SIZE" kind="output" name="delta"/>
            <port annotation="WRITE_ONLY"
                expr="GRID_SIZE*GRID_SIZE" kind="output" name="psi"/>
            <port annotation="WRITE_ONLY"
                expr="GRID_SIZE*GRID_SIZE" kind="output" name="save"/>
        </node>
        <node expr="MAX_SOURCES" getter=""
            id="psi_source_list__delta_source_list" kind="delay"
            level="permanent" setter="">
            <port annotation="NONE" expr="MAX_SOURCES"
                kind="input" name="set"/>
            <port annotation="NONE" expr="MAX_SOURCES"
                kind="output" name="get"/>
        </node>
        <node expr="1" getter=""
            id="psi_num_sources_out__delta_num_sources_in"
            kind="delay" level="permanent" setter="">
            <port annotation="NONE" expr="1" kind="input" name="set"/>
            <port annotation="NONE" expr="1" kind="output" name="get"/>
        </node>
        <node expr="GRID_SIZE*GRID_SIZE" getter=""
            id="Broadcast_image_estimate_delta__delta_image_estimate"
            kind="delay" level="permanent" setter="">
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="input" name="set"/>
            <port annotation="NONE" expr="GRID_SIZE*GRID_SIZE"
                kind="output" name="get"/>
        </node>
        <edge expr="MAX_SOURCES" kind="fifo" source="psi"
            sourceport="source_list" target="delta"
            targetport="source_list" type="PRECISION3">
            <data key="delay">psi_source_list__delta_source_list</data>
        </edge>
        <edge expr="1" kind="fifo" source="psi"
            sourceport="num_sources_out" target="delta"
            targetport="num_sources_in" type="int">
            <data key="delay">psi_num_sources_out__delta_num_sources_in</data>
        </edge>
        <edge expr="GRID_SIZE*GRID_SIZE" kind="fifo"
            source="Broadcast_image_estimate" sourceport="delta"
            target="delta" targetport="image_estimate" type="PRECISION">
            <data key="delay">Broadcast_image_estimate_delta__delta_image_estimate</data>
        </edge>
        <edge kind="fifo" source="Broadcast_image_estimate"
            sourceport="save" target="save_output"
            targetport="model" type="PRECISION"/>
        <edge kind="fifo" source="Broadcast_residual"
            sourceport="psi" target="psi"
            targetport="delta_image" type="PRECISION"/>
        <edge kind="fifo" source="Broadcast_residual"
            sourceport="save" target="save_output"
            targetport="residual" type="PRECISION"/>
        <edge kind="fifo" source="iterator"
            sourceport="cycle_out" target="Broadcast_cycle"
            targetport="input" type="int"/>
        <edge kind="fifo" source="Broadcast_cycle"
            sourceport="psi" target="psi" targetport="cycle" type="int"/>
        <edge kind="fifo" source="Broadcast_cycle"
            sourceport="save" target="save_output"
            targetport="cycle" type="int"/>
        <edge kind="fifo" source="psi"
            sourceport="image_estimate"
            target="Broadcast_image_estimate" targetport="input" type="PRECISION"/>
        <edge kind="fifo" source="delta" sourceport="delta_image"
            target="Broadcast_residual" targetport="input" type="PRECISION"/>
        <edge kind="fifo" source="delta" sourceport="image_out"
            target="psi" targetport="input_model" type="PRECISION"/>
        <edge kind="fifo" source="Broadcast_cycle"
            sourceport="delta" target="delta" targetport="cycle" type="int"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="config" target="Broadcast_config"
            targetport="input" type="Config"/>
        <edge kind="fifo" source="Broadcast_config"
            sourceport="delta" target="delta" targetport="config" type="Config"/>
        <edge kind="fifo" source="Broadcast_config"
            sourceport="psi" target="psi" targetport="config" type="Config"/>
        <edge kind="fifo" source="Broadcast_config"
            sourceport="save" target="save_output"
            targetport="config" type="Config"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="clean_psf" target="save_output"
            targetport="clean_psf" type="PRECISION"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="psf_halfdims"
            target="Broadcast_psfhalfdims" targetport="input" type="int2"/>
        <edge kind="fifo" source="Broadcast_psfhalfdims"
            sourceport="psi" target="psi"
            targetport="partial_psf_halfdims" type="int2"/>
        <edge kind="fifo" source="Broadcast_psfhalfdims"
            sourceport="save" target="save_output"
            targetport="clean_psf_halfdims" type="int2"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="gridding_kernel_supports" target="delta"
            targetport="gridding_kernel_supports" type="int2"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="gridding_kernels" target="delta"
            targetport="gridding_kernels" type="PRECISION2"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="psf" target="Broadcast_psf"
            targetport="input" type="PRECISION"/>
        <edge kind="fifo" source="Broadcast_psf"
            sourceport="delta" target="delta" targetport="psf" type="PRECISION"/>
        <edge kind="fifo" source="Broadcast_psf" sourceport="psi"
            target="psi" targetport="psf" type="PRECISION"/>
        <edge kind="fifo" source="Broadcast_psf"
            sourceport="save" target="save_output"
            targetport="psf" type="PRECISION"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="receiver_pairs" target="delta"
            targetport="receiver_pairs" type="int2"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="vis_uvw_coords" target="delta"
            targetport="vis_coords" type="PRECISION3"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="degridding_kernel_supports"
            target="delta"
            targetport="degridding_kernel_supports" type="int2"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="degridding_kernels" target="delta"
            targetport="degridding_kernels" type="PRECISION2"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="measured_visibilities" target="delta"
            targetport="measured_vis" type="PRECISION2"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="prolate" target="delta"
            targetport="prolate" type="PRECISION"/>
        <edge kind="fifo" source="setup_ri_pipeline"
            sourceport="gains" target="delta" targetport="gains" type="PRECISION2"/>
        <edge kind="dependency" source="GRID_SIZE" target="psi" targetport="GRID_SIZE"/>
        <edge kind="dependency" source="NUM_MINOR_CYCLES"
            target="psi" targetport="NUM_MINOR_CYCLES"/>
        <edge kind="dependency" source="GRID_SIZE"
            target="save_output" targetport="GRID_SIZE"/>
        <edge kind="dependency" source="GRID_SIZE"
            target="Broadcast_image_estimate" targetport="GRID_SIZE"/>
        <edge kind="dependency" source="GRID_SIZE"
            target="Broadcast_residual" targetport="GRID_SIZE"/>
        <edge kind="dependency" source="NUM_MAJOR_CYCLES"
            target="iterator" targetport="ITER"/>
        <edge kind="dependency" source="Zero" target="iterator" targetport="START"/>
        <edge kind="dependency" source="NUM_RECEIVERS" target="NUM_BASELINES"/>
        <edge kind="dependency" source="NUM_MINOR_CYCLES" target="MAX_SOURCES"/>
        <edge kind="dependency" source="MAX_SOURCES" target="psi" targetport="MAX_SOURCES"/>
        <edge kind="dependency"
            source="TOTAL_DEGRIDDING_KERNEL_SAMPLES"
            target="delta" targetport="TOTAL_DEGRIDDING_KERNEL_SAMPLES"/>
        <edge kind="dependency"
            source="TOTAL_GRIDDING_KERNEL_SAMPLES" target="delta" targetport="TOTAL_GRIDDING_KERNEL_SAMPLES"/>
        <edge kind="dependency" source="NUM_VISIBILITIES"
            target="delta" targetport="NUM_VISIBILITIES"/>
        <edge kind="dependency" source="NUM_GRIDDING_KERNELS"
            target="delta" targetport="NUM_GRIDDING_KERNELS"/>
        <edge kind="dependency" source="NUM_DEGRIDDING_KERNELS"
            target="delta" targetport="NUM_DEGRIDDING_KERNELS"/>
        <edge kind="dependency" source="GRID_SIZE" target="delta" targetport="GRID_SIZE"/>
        <edge kind="dependency" source="MAX_SOURCES"
            target="delta" targetport="MAX_SOURCES"/>
        <edge kind="dependency" source="NUM_RECEIVERS"
            target="delta" targetport="NUM_RECEIVERS"/>
        <edge kind="dependency" source="NUM_BASELINES"
            target="delta" targetport="NUM_BASELINES"/>
        <edge kind="dependency" source="OVERSAMPLING_FACTOR"
            target="delta" targetport="OVERSAMPLING_FACTOR"/>
        <edge kind="dependency" source="NUM_GRIDDING_KERNELS"
            target="setup_ri_pipeline" targetport="NUM_GRIDDING_KERNELS"/>
        <edge kind="dependency" source="GRID_SIZE"
            target="setup_ri_pipeline" targetport="GRID_SIZE"/>
        <edge kind="dependency" source="GAUSSIAN_CLEAN_PSF"
            target="setup_ri_pipeline" targetport="GAUSSIAN_CLEAN_PSF"/>
        <edge kind="dependency" source="NUM_RECEIVERS"
            target="setup_ri_pipeline" targetport="NUM_RECEIVERS"/>
        <edge kind="dependency" source="NUM_BASELINES"
            target="setup_ri_pipeline" targetport="NUM_BASELINES"/>
        <edge kind="dependency"
            source="TOTAL_GRIDDING_KERNEL_SAMPLES"
            target="setup_ri_pipeline" targetport="TOTAL_GRIDDING_KERNEL_SAMPLES"/>
        <edge kind="dependency" source="NUM_VISIBILITIES"
            target="setup_ri_pipeline" targetport="NUM_VISIBILITIES"/>
        <edge kind="dependency"
            source="TOTAL_DEGRIDDING_KERNEL_SAMPLES"
            target="setup_ri_pipeline" targetport="TOTAL_DEGRIDDING_KERNEL_SAMPLES"/>
        <edge kind="dependency" source="NUM_DEGRIDDING_KERNELS"
            target="setup_ri_pipeline" targetport="NUM_DEGRIDDING_KERNELS"/>
        <edge kind="dependency" source="GRID_SIZE"
            target="Broadcast_psf" targetport="GRID_SIZE"/>
        <edge kind="dependency" source="MAX_SOURCES" target="psi_source_list__delta_source_list"/>
        <edge kind="dependency" source="GRID_SIZE" target="Broadcast_image_estimate_delta__delta_image_estimate"/>
        <edge kind="dependency" source="NUM_MAJOR_CYCLES" target="MAX_SOURCES"/>
        <edge kind="dependency" source="SLICES" target="delta" targetport="SLICES"/>
    </graph>
</graphml>
