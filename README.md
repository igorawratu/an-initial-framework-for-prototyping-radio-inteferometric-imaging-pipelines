# an-initial-framework-for-prototyping-radio-inteferometric-imaging-pipelines
This repository is primarily used to house the code for this paper. For this reason, the code will largely remain unchanged, bar bugfixes.

## Paper Data
One can find the data used for the paper, such as the time and memory results, the validation images, and the benchmark data along with the fitted polynomials in the DASIP_Paper_data directory.

## Data-flow diagrams and Code
One can find our data-flow diagrams in the Algo/ directory, with the top-level diagrams being named generic_top_*.pi. Our code is located in the Code/ directory
